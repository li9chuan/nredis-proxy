/**
 * 
 */
package com.opensource.netty.redis.proxy.spring.schema.support;

import java.io.Serializable;

import com.opensource.netty.redis.proxy.core.config.RedisPoolConfig;

/**
 * @author liubing
 *
 */
public class RedisProxySlave implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2087897700756695413L;
    
	private String host;//主机名
	
	private int port;//端口号
		
	private int weight=1;//默认权重比例为1
	
	private RedisPoolConfig redisPoolConfig;
	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}
	

	/**
	 * @return the key
	 */
	public String getKey() {
		StringBuilder stringBuilder=new StringBuilder();
		stringBuilder.append(host).append("-").append(port);
		return stringBuilder.toString();
	}

	/**
	 * @return the redisPoolConfig
	 */
	public RedisPoolConfig getRedisPoolConfig() {
		return redisPoolConfig;
	}

	/**
	 * @param redisPoolConfig the redisPoolConfig to set
	 */
	public void setRedisPoolConfig(RedisPoolConfig redisPoolConfig) {
		this.redisPoolConfig = redisPoolConfig;
	}
	
}
